/**
 * Important : config file must be created with your own values with the following shape :
 *
 * ```
 * module.exports {
 *   clientID: 'your_vimeo_clientID',
 *   clientSecret: 'your_vimeo_clientSecret',
 *
 *   // Required scopes : public private purchased create edit delete interact upload video_files
 *   accessToken: 'your_vimeo_token',
 *
 *   VIDEO_ID: 'one_of_your_videos_id,
 * }
 *
 */
const config = require('./_config')
const Vimeo  = require('../index').Vimeo

module.exports = () => new Vimeo(config.clientID, config.clientSecret, config.accessToken)