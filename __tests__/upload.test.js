const R      = require('ramda')
const Promise= require('bluebird')
const assert = require('assert')
const vim    = require('./_init')()
const fs     = require('fs')

const path   = require('path')
const videoPath = path.join(process.cwd(), '__tests__', 'data', 'fishes.mp4')

function deleteVideo(videoId) {
  return new Promise(function (resolve, reject) {
    var options = {
      method: 'DELETE',
      path: '/videos/' + videoId
    }

    return vim.request(options, function (error, body, status_code /*, headers*/) {
      if (status_code == '404') return resolve(0)
      if (error) return reject(error)
      return resolve(1)
    })
  })
}


describe('Upload video', () => {

  let videoId

  test('Upload from file path', () => {

    return vim.uploadStream(fs.createReadStream(videoPath))
      .then( res => {
        const {
          videoId:receivedId,
          body,
        } = res
        videoId = receivedId

        assert(!!videoId, 'Video uploaded')
      })

  })

  test('Delete newly uploaded video', () => {

    return deleteVideo(videoId)
      .then( res => {
        assert(!!res, 'Video deleted')
      })
  })


})