const R      = require('ramda')
const assert = require('assert')
const vim    = require('./_init')()
const { VIDEO_ID } = require('./_config')
const resolveDownloadLinks = require('../lib/_resolve_download_link')

let infos

const getInfos = (videoId) => (
  vim.info(VIDEO_ID)
)

describe('Get Informations from video Id', () => {

  let infos
  beforeAll( done => (
    getInfos(VIDEO_ID)
      .then( (res) => {
        infos = res.body
        done()
      })
    ))

  test('Get video infos', () => {
    assert(!!infos, 'Can request video')
  })

  test('Get video pictures', () => {
    assert(infos.pictures.sizes.length > 0, 'Result has thumbnails')
  })

  test('Get download links', () => {
    assert(infos.download.length > 0, 'Result has thumbnails')
  })

  test('Get default download link', () => {
    resolveDownloadLinks(infos.download)
      .then( res => {
        assert(res.quality === 'source', 'Get default link for download')
      })
  })

  test('Get specific download link', () => {
    resolveDownloadLinks(infos.download, 'mobile')
      .then( res => {
        console.log(res.quality)
        assert(res.quality === 'mobile', 'Get specific link for download')
      })
  })


})