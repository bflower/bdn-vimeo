var Promise             = require('bluebird'),
    hyperquest          = require('hyperquest'),
    resolveDownloadLink = require('./_resolve_download_link.js');

function duplicate(videoId) {
  var isHD  = false,
      _this = this,
      name, description;

  return this.info(videoId)
    .then(function(props) {
      name        = props.body.name;
      description = props.body.description;
      return resolveDownloadLink(props.body.download);
    })

    .then(function(linkObj) {
      isHD = linkObj.quality == 'hd';
      return hyperquest(linkObj.link);
    })

    .then(function(req) {
      var opts = {
            upTo1080   : isHD,
            name       : name,
            description: description
          };

      // request is hyperquest request - can be piped to uploadStream
      return _this.uploadStream(req, opts);
    });
}

module.exports = duplicate;
