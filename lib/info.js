
var Promise = require('bluebird');

module.exports = info;

function info(videoId) {
  var _this = this;
  return new Promise(function(resolve, reject) {
    _this.request(
      {
        path: `/videos/${videoId}`,
      },
      function (error, body, statusCode, headers) {

      if (statusCode == '404') {
        console.error('Vimeo : Video not found (id : ' + videoId + ')');
        return reject(new Error('Vimeo : Video not found (id : ' + videoId + ')'));
      }

      if (error) return reject(error);

      resolve({
        body,
        videoId,
        res: {
          headers,
          statusCode,
        }
      });

    })
  });
}
