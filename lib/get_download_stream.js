var hyperquest           = require('hyperquest')
    resolveDownloadLink  = require('./_resolve_download_link.js');


/**
 * return a stream that can be downloaded or pipe to a web response
 *
 * @param  {String} videoId
 * @param  {String} quality   - Prefered quality, original|source|hd|sd|mobile
 * @return {Promise<stream.Readable>}         [description]
 */
function getDownloadStream(videoId, quality) {
  var _this = this;

  return this.info(videoId)
    .then(function(props) {
      return resolveDownloadLink(props.body.download, quality);
    })
    .then(function(linkObj) {
      return hyperquest(linkObj.link);
    })
}

module.exports = getDownloadStream;
